﻿Plataforma: Android
Engine: Unity 2017.3.1f1
Linguagem: C#

Sobre:

Este jogo pretende despertar a curiosidade e o conhecimento sobre artrópodes brasileiros (carrapatos e insetos) que são vetores de agentes patogênicos que circulam entre animais silvestre, domésticos e pessoas.
A montagem dos vetores em 3D, busca mostrar o quanto é complexa a evolução não só dos vetores, mas principalmente da transmissão de microrganismos. 
Os vetores fazem parte da natureza e com incrível capacidade de adaptação usam o sangue de animais para se alimentar e amadurecer seus ovos. A picada entre um animal e outro pode transmitir microrganismos que circulam entre eles. Alguns animais e pessoas ficam doentes, outros não. Isso porque, alguns animais e pessoas são resistentes, alguns microrganismos são mais virulentos do que outros e ainda, outras possibilidades.
Normalmente os vetores vivem nas áreas naturais, mas quando os ambientes são destruídos e os animais desaparecem vão em busca de sangue humano e de animais domésticos, e assim surgem o surto de diversas doenças. 
Mas quais são os vetores mais importantes no Brasil? Que doenças são essas e onde elas estão?
Encontre essas e outras respostas em www.biodiversidade.ciss.fiocruz.br
