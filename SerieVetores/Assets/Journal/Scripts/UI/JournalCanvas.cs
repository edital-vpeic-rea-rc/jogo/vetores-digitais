﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameGrind
{
    public class JournalCanvas : MonoBehaviour
    {
        [SerializeField]
        private KeyCode togglePanel;
        private AchievementUIList panel;

        void Awake()
        {
            if (FindObjectsOfType<JournalCanvas>().Length > 1)
            {
                Destroy(this.gameObject);
                Debug.LogWarning("Deleted duplicate instance of Journal. Journal should only be installed in the first scene you load achievements in and not in a scene that's revisted often.");
            }
            // We're finding the list by name as a child to make sure we find it even if it isn't disabled
            panel = transform.Find("Achievement_UI_List").GetComponent<AchievementUIList>();
            DontDestroyOnLoad(this);
        }
        void OnEnable() {
            //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
            SceneManager.sceneLoaded += OnLevelFinishedLoading;
        }

        void OnDisable() {
            //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
            SceneManager.sceneLoaded -= OnLevelFinishedLoading;
        }

        void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode) {
            //Debug.Log("Level Loaded");
            //Debug.Log(scene.name);
            //Debug.Log(mode);
            if (SceneManager.GetActiveScene().buildIndex > 1) {
                GetComponent<Canvas>().worldCamera = GameObject.Find("SlotCamera").GetComponent<Camera>();
                GetComponent<Canvas>().planeDistance = 80;
            } else {
                GetComponent<Canvas>().worldCamera = Camera.main;
            }   
        }
        private void Update()
        {
            if (Input.GetKeyDown(togglePanel))
            {
                if (panel != null)
                    ToggleAchievementPanel();
            }
        }

        public void ToggleAchievementPanel()
        {
            panel.TogglePanel();
        }
    }
}
