﻿using UnityEngine;
using UnityEditor;

public static class DebugMenu {
    [MenuItem("Debug/Print Position")]
    public static void PrintGlobalPosition() {
        if (Selection.activeGameObject != null) {
            Debug.Log(Selection.activeGameObject.name + " Global position: " + Selection.activeGameObject.transform.position.ToString("F4") );
        }
    }

    [MenuItem("Debug/Delete Prefs")]
    public static void DeleteSave() {
        PlayerPrefs.DeleteAll();
        Debug.Log("Save deleted");
    }
}
