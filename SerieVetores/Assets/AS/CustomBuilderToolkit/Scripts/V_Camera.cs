﻿using UnityEngine;
using System.Collections;

/// <summary>
/// "Camera Script for CustomBuilderToolkit"
/// 
/// This holds the necessary controls for the
/// camera such as rotation with mouse and zooming in
/// the workshop editor scene.
/// 
///       Visyde Interactives | 2017
/// </summary>

public class V_Camera : MonoBehaviour
{

	// Public variables:
	public Transform centerTransform;
	public float distance = 5.0f;
	public float xSpeed = 120.0f;
	public float ySpeed = 120.0f;
	public float yMinLimit = -20f;
	public float yMaxLimit = 80f;
	public float distanceMin = .5f;
	public float distanceMax = 15f;
	public float smoothTime = 2f;

	public float ClampAngle(float angle, float min, float max)
	{
		if (angle < -360F) {
			angle += 360F;
		}
		if (angle > 360F) {
			angle -= 360F;
		}
		return Mathf.Clamp(angle, min, max);
	}

	// Private variables:
	private float rotationYAxis = 0.0f;
	private float rotationXAxis = 0.0f;
	private float velocityX = 0.0f;
	private float velocityY = 0.0f;

	// Use this for initialization
	void Start()
	{
		Vector3 angles = transform.eulerAngles;
		rotationYAxis = angles.y;
		rotationXAxis = angles.x;
	}


	void LateUpdate()
	{
		if (centerTransform != null)
		{
			if (Input.GetMouseButton(2))
			{
				velocityX += xSpeed * Input.GetAxis("Mouse X") * distance * 0.02f;
				velocityY += ySpeed * Input.GetAxis("Mouse Y") * 0.02f;
			}
			rotationYAxis += velocityX;
			rotationXAxis -= velocityY;
			rotationXAxis = ClampAngle(rotationXAxis, yMinLimit, yMaxLimit);
			Quaternion toRotation = Quaternion.Euler(rotationXAxis, rotationYAxis, 0);
			Quaternion rotation = toRotation;

			distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);
			Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
			Vector3 position = rotation * negDistance + centerTransform.position;

			transform.rotation = rotation;
			transform.position = position;
			velocityX = Mathf.Lerp(velocityX, 0, Time.deltaTime * smoothTime);
			velocityY = Mathf.Lerp(velocityY, 0, Time.deltaTime * smoothTime);
		}
	}
}