﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// "RootPart Script for CustomBuilderToolkit"
/// 
/// This recieves inputs from editor for an attachment...
/// 
///       Visyde Interactives | 2017
/// </summary>

[RequireComponent(typeof(Rigidbody))]
public class V_RootPart : MonoBehaviour {

	Rigidbody rg;

	// Use this for initialization
	void Start () {
		// Prevent all physics:
		rg = GetComponent<Rigidbody> ();
		rg.isKinematic = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseEnter(){
		//GetComponentInParent<V_Attachment> ().CallMouseEnter ();
	}

	void OnMouseExit(){
		//GetComponentInParent<V_Attachment> ().CallMouseExit ();
	}

	void OnMouseOver(){
		if (Input.GetMouseButtonDown (1)) {
			//GetComponentInParent<V_Attachment> ().Remove ();
		}
	}
}
