﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// "Framework Script for CustomBuilderToolkit"
/// 
/// This must be attached to all base framework/body
/// gameObjects to be built on.
/// 
///       Visyde Interactives | 2017
/// </summary>

public class V_Framework : MonoBehaviour {

	public V_SlotData[] data;

	// Check if this object is currently in game:
	//public bool isInGame = false;
	//public GameObject[] gameOnly;
	//public GameObject[] workshopOnly;

	void Awake(){
		// Initialize the id's of the slots:
		//if (data.Length > 0) {
		//	foreach (V_SlotData slot in data) {
		//		slot.slot.id = System.Array.IndexOf (data, slot);
		//	}
		//}
	}

	//void Update(){
	//	// If we're not in game:
	//	if (!isInGame) {
	//		if (gameOnly [0]) {
	//			if (gameOnly [0].activeSelf) {
	//				foreach (GameObject ob in gameOnly) {
	//					ob.SetActive (false);
	//				}
	//				foreach (GameObject ob in workshopOnly) {
	//					ob.SetActive (true);
	//				}
	//			}
	//		}
	//	}
	//	// But if we are:
	//	else {
	//		if (workshopOnly [0]) {
	//			if (workshopOnly [0].activeSelf) {
	//				foreach (GameObject ob in workshopOnly) {
	//					ob.SetActive (false);
	//				}
	//				foreach (GameObject ob in gameOnly) {
	//					ob.SetActive (true);
	//				}
	//			}
	//		}
	//	}
	//}

}
