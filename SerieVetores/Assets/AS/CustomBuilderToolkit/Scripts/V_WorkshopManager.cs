﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;
using Fungus;

/// <summary>
/// "Workshop Script for CustomBuilderToolkit"
/// 
/// This is what makes the workshop editor working.
/// Only one instance of this object is needed in a scene!
/// 
///       Visyde Interactives | 2017
/// </summary>

public class V_WorkshopManager : MonoBehaviour {

    
    public static V_Attachment curHolding;

    public Flowchart flow;

    [Header("Puzzle")]
    public GameObject animal;
    public V_Attachment[] pieces;
    public List<V_Slot> slots = new List<V_Slot>();
    public LineRenderer line;

    public int[] curiositiesTriggerPoint;

    int totalPieces;
    int numOfPiecesAttached;
    public string puzzleCompleteKey = "CarrapatoCompleto";
    public string unlockedVetor = "Barbeiro";
    //Score 
    [Header("Score System")]
    public string finalScoreKey = "CarrapatoScore";
    public GameObject[] stars;
    public float oneStartScore, twoStarScore, threeStartScore;

    public float finalScore;
    int starsGained;
    int lastStarsGained;
    int hintsUsed;

    [Header("Save IDs")]
    public int levelCompletionID = 0;
    public int hintsUsedID = 1;
    public int numStarsID = 2;

    [Header("UI")]
    public Text progressionText;
    public ProgressBarPro progressBar;

    [Header("Audio")]
    public string CompleteAudio;

    [SerializeField]
    public UnityEvent onPuzzleStart = new UnityEvent();

    [SerializeField] 
    public UnityEvent onPuzzleCompleted = new UnityEvent(); 

	void Start () {

        if (ES3.KeyExists(finalScoreKey))
            lastStarsGained = ES3.Load<int>(finalScoreKey);
        else
            lastStarsGained = 0;

        totalPieces = pieces.Length;
        //progressionText.text = numOfPiecesAttached + "/" + totalPieces;
        //progressBar.maxValue = totalPieces;
        //progressBar.value = numOfPiecesAttached;
        progressBar.SetValue(numOfPiecesAttached, totalPieces);
        //StartCoroutine(StartPuzzle());
    }
    public void Initialize() {
        onPuzzleStart.Invoke();
    }

    public void AddPiece() {
        numOfPiecesAttached++;
        progressBar.SetValue(numOfPiecesAttached, totalPieces);

        CheckForCuriosity();
        CheckIfObjectComplete();
    }

    void CheckForCuriosity() {
        for (int i = 0; i < curiositiesTriggerPoint.Length; i++) {
            if(curiositiesTriggerPoint[i] == numOfPiecesAttached) {
                flow.ExecuteBlock("Curiosidade" + i);
            }
        }
    }

    public void TriggerHint() {
        foreach (V_Slot slot in slots) {
            if (slot.gameObject.activeInHierarchy && slot.gameObject.GetComponent<MeshRenderer>().enabled) {
                hintsUsed++;
                foreach (V_Attachment piece in pieces) {
                    if(piece.uniqueID == slot.id) {
                        //Debug.Log("Piece with same ID as slot: " + piece.gameObject.name);
                        //Debug.Log("show the hint for ID = " + slot.id);
                        //I need to call this from a button and call function on slot and piece to make them blink
                        slot.BlinkMaterial();
                        piece.BlinkMaterial();
                        LineRenderer hintLine = Instantiate(line, piece.transform.position, Quaternion.identity);
                        hintLine.SetPosition(0, piece.transform.GetComponentInChildren<Renderer>().bounds.center);
                        hintLine.SetPosition(1, slot.transform.GetComponent<Renderer>().bounds.center);
                        hintLine.material.DOOffset(new Vector2(-5,0), 1).SetLoops(-1,LoopType.Incremental);
                        hintLine.DOColor(new Color2(Color.white, Color.white), new Color2(Color.clear, Color.clear), 1);
                        Destroy(hintLine.gameObject, 1);
                        
                    }
                }

            }
        }
    }

    public void CheckIfObjectComplete() {
        if(numOfPiecesAttached >= totalPieces) {
            //If it´s the first time completing the puzzle, then gain achievement and save 
            GameGrind.Journal.Increment(levelCompletionID, 1);

            ES3.Save<string>(unlockedVetor, unlockedVetor);

            //Increment total puzzles
            if (!ES3.KeyExists(puzzleCompleteKey))
                GameGrind.Journal.Increment("Vetores montados!", 1);
            
                ES3.Save<string>(puzzleCompleteKey, puzzleCompleteKey);

            SoundManager.PlaySFX(CompleteAudio);
            FinalScore();
            onPuzzleCompleted.Invoke();
        }
    }

    public void FinalScore() {

        CalculateFinalScore();
        //* = 25, ** = 20, *** = 15
        // 18
        if (finalScore > oneStartScore) {
            //no stars
            starsGained = 0;
        } else if((finalScore <= oneStartScore) && (finalScore > twoStarScore)) {
            //one star
            starsGained = 1;
            stars[0].SetActive(true);
        } else if ((finalScore <= twoStarScore) && (finalScore > threeStartScore)) {
            //two stars
            starsGained = 2;
            stars[0].SetActive(true);
            stars[1].SetActive(true);
        } else if (finalScore <= threeStartScore) {
            //three stars
            starsGained = 3;
            stars[0].SetActive(true);
            stars[1].SetActive(true);
            stars[2].SetActive(true);
            GameGrind.Journal.Increment(numStarsID, 1);
        }
        
        if(starsGained > lastStarsGained) {
            ES3.Save<int>(finalScoreKey, starsGained);
            //Save total stars
            GameGrind.Journal.Increment("Mestre dos Vetores!", starsGained - lastStarsGained);
        }

        if(hintsUsed <= 0) {
            GameGrind.Journal.Increment(hintsUsedID, 1);
        }
    }

    public void CalculateFinalScore() {
        finalScore = Timer.instance.timer + hintsUsed*2;
    }
}
