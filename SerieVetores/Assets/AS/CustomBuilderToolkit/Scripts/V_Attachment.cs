﻿using DG.Tweening;
using Lean.Touch;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// "Attachment Script for CustomBuilderToolkit"
/// 
/// All attachment objects must have this component.
/// 
///       Visyde Interactives | 2017
/// </summary>

public class V_Attachment : MonoBehaviour {

	[Header("Type:")]

	public int uniqueID;          // This is set automatically by the GameManager if it is assigned to V_GameManager.attachments
    public bool hasSlots = false;
	[Header("Materials:")]
	public Material dragged;      // Material when dragging
	public Material clamped;      // Material when clamped to a slot
	public Material attached;     // Material when attached to a slot
	public Material highlighted;  // Material when highlighted by the cursor

	public Material current;      //

    [Header("Audio")]
    public string attachedAudio;
    public string selectedAudio;
    public string deselectedAudio;

    public Collider col;         // The collider for this attachment

	//[HideInInspector]
	public bool isAttached = false;
	//[HideInInspector]
	public bool isClamped = false;
	//[HideInInspector]
	public V_Slot target;        // The currently target slot

    [HideInInspector]
    public Button SpawnedBtn;

    public GameObject smokeParticle;

    [HideInInspector]
    public Renderer rend;
    // Some private variables:
	private Vector3 side;
    private Vector3 lastPos;
    private Vector3 initPos;
    private Vector3 initRot;
    private bool isHighlighted = false;
	
	private V_WorkshopManager ws;
    public bool beingDragged;

    [SerializeField]
    public UnityEvent onAttached = new UnityEvent();

    Rigidbody rb;

    void Start () {
        //reference to the rigidbody
        rb = GetComponent<Rigidbody>();

		// Get a reference of the Renderer component for later use:
		rend = GetComponentInChildren<Renderer> ();
		
		// get a reference of the workshop manager component:
		ws = FindObjectOfType (typeof(V_WorkshopManager)) as V_WorkshopManager;

        //Deactivate Slots if any
        SetSlotsActive(false);

        //Get initial position and rotation
        initPos = transform.position;
        initRot = transform.eulerAngles;

        //Play montado animation 
        if (GetComponent<Animator>())
            GetComponent<Animator>().Play("Desmontado");
    }

    public void Clamp() {
            // Change to "clamped" material:
        rend.material = clamped;
            //rb.isKinematic = true;
        transform.SetParent(target.transform);
        //transform.rotation = target.slotedRotation;
        //transform.position = target.slotedPosition;
        transform.parent = null;
        
    }
    public void UnClamp() {
            // Change to "dragged" material:
            rend.material = dragged;
            //DragToMouse ();
            V_WorkshopManager.curHolding = this;
            rb.isKinematic = true;
        
    }
    public void OnBeingDragged() {
        beingDragged = true;
        SoundManager.PlaySFX(selectedAudio);
        UnClamp();
        CursorManager.cursorManager.SetHoldingCurso();
    }

    public void OnRelease() {
        CursorManager.cursorManager.SetNormalCurso();
        // If we are hovering a slot:
        if (target != null) {
            lastPos = target.transform.position;
            rb.isKinematic = false;
            if (clamped) {
                beingDragged = false;
                Attach();
            }

        } else {
            SoundManager.PlaySFX(deselectedAudio);
            rb.isKinematic = false;
            transform.position = initPos;
            transform.eulerAngles = initRot;
            Instantiate(smokeParticle, initPos, Quaternion.identity);
        }

        V_WorkshopManager.curHolding = null;
        //GetComponent<Rigidbody>().isKinematic = false;
        // Change the material:
        if (!isHighlighted) {
            rend.material = attached;
        } else {
            rend.material = highlighted;
        }

        col.enabled = true;
        V_Slot inSlot = GetComponentInParent<V_Slot>();
        beingDragged = false;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        
    }

	public void Attach(){
        rb.isKinematic = true;
        GetComponent<LeanSelectableDragRigidbody3D>().enabled = false;
        GetComponent<LeanSelectable>().enabled = false;
        col.enabled = false;
        isAttached = true;
        isClamped = false;
        target.pieceHoverring = false;
        target.StopCoroutine(target.SlotedFeedback());
        transform.SetParent(target.transform);
        transform.rotation = target.slotedRotation;
        transform.position = target.slotedPosition;
        target.rend.enabled = false;

        V_WorkshopManager.curHolding = null;
        gameObject.layer = 2;
        SoundManager.PlaySFX(attachedAudio);
        ws.AddPiece();
        if (hasSlots)
            SetSlotsActive(true);
        //DisableButtonOnAttached();
        //ws.CheckIfObjectComplete();
        //this.enabled = false;
        onAttached.Invoke();
    }

    void SetSlotsActive(bool active) {
        foreach (Transform child in transform) {
            if(child.tag == "Slot") {
                child.gameObject.SetActive(active);
            }
        }
    }

    public void BlinkMaterial() {
        StartCoroutine(Blink());
    }
    IEnumerator Blink() {      
        rend.material = clamped;
        yield return new WaitForSeconds(1);
        rend.material = current;
    }
}
