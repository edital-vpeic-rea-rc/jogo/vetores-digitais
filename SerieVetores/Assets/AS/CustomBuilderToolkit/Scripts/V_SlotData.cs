﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// V_SlotData for framework's slot data...
/// </summary>

[System.Serializable]
public class V_SlotData {

	public V_Slot slot;
	public int attachment;
}
