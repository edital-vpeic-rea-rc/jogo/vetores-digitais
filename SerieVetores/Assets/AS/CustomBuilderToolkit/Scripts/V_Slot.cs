﻿using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class V_Slot : MonoBehaviour {

    [Header("Properties:")]
    public int id;

    public Vector3 slotedPosition;
    public Quaternion slotedRotation;

    public bool pieceHoverring;

	[Header("Apearance:")]
	public Material normal;
	public Material highlighted;
	//public Material error;

    [HideInInspector]
	public Renderer rend;
	Rigidbody rg;

    void Start () {
		// Get a reference of the Renderer component for later use:
		rend = GetComponent<Renderer> ();

		// Prevent all physics:
		rg = GetComponent<Rigidbody> ();
		rg.isKinematic = true;
	}
    private void Awake() {
        MMVibrationManager.iOSInitializeHaptics();
    }
    //void OnMouseExit() {
    //    rend.material = normal;
    //    if (transform.childCount < 2) {
    //        V_Attachment target = V_WorkshopManager.curHolding;
    //        if (target != null) {
    //            target.isClamped = false;
    //            target.target = null;
    //        }
    //    }
    //}

    private void OnTriggerEnter(Collider other) {
      if(other.tag == "Piece") {
            if (!pieceHoverring) {
                //Debug.Log(other);
                if (transform.childCount < 2) {
                    V_Attachment target = V_WorkshopManager.curHolding;
                    if (target != null) {
                        if (target.uniqueID == id) {
                            target.isClamped = true;
                            target.target = this;
                            target.Clamp();
                            pieceHoverring = true;
                            //rend.material = highlighted;
                            StartCoroutine(SlotedFeedback());
                        } else {
                            //rend.material = normal;
                        }
                    }

                }
            }       
        }
        
    }

    private void OnTriggerExit(Collider other) {
        if (other.tag == "Piece") {
            if (pieceHoverring) {
                //Debug.Log(other);
                //rend.material = normal;
                if (transform.childCount < 2) {
                    V_Attachment target = V_WorkshopManager.curHolding;
                    if (target != null) {
                        target.isClamped = false;
                        target.rend.material = target.dragged;
                        target.target = null;
                        pieceHoverring = false;
                        StopCoroutine(SlotedFeedback());
                        //rend.material = normal;
                    }
                }
            }              
        }
            
    }

    public void BlinkMaterial() {
        StartCoroutine(Blink());
    }
    IEnumerator Blink() {
        rend.material = highlighted;
        yield return new WaitForSeconds(1);
        rend.material = normal;
    }

    public IEnumerator SlotedFeedback() {
        while (pieceHoverring) {
            SoundManager.PlaySFX("bop");
            MMVibrationManager.Vibrate();
            yield return new WaitForSeconds(0.5f);
        }    
    }

    void OnDisable() {
        MMVibrationManager.iOSReleaseHaptics();
    }
}
