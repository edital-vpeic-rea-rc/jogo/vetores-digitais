﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Smooth Camera for the DEMO.
/// </summary>

public class V_DemoCamera : MonoBehaviour {

	public Transform target;
	public float distance = 5.0f;
	public float height = 10f;

	public float followSpeed = 2.0f;

	// We'll use FixedUpdate to stop jittering (this is because we follow a PHYSICS object which also runs in FixedUpdate)...
	void FixedUpdate () {
		if (!target) {
			// Find the player in the scene:
			V_Framework Ftarget = FindObjectOfType (typeof(V_Framework)) as V_Framework;
			if (Ftarget) {
				target = Ftarget.transform;
			}
		} else {
			// Smoothly follow the target (player framework):
			Vector3 pos = transform.position;
			pos = Vector3.Lerp (pos, new Vector3 (target.transform.position.x, target.transform.position.y+height, target.transform.position.z - distance), followSpeed * Time.deltaTime);
			transform.position = pos;

			transform.LookAt (target);
		}
	}
}