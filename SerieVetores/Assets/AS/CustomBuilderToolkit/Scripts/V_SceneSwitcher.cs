﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// "SceneSwitcher Script for CustomBuilderToolkit"
/// 
/// This is used for switching between scenes (for demo porpouses)...
/// 
///       Visyde Interactives | 2017
/// </summary>

[DisallowMultipleComponent]
public class V_SceneSwitcher : MonoBehaviour {

	[Header("The scene for in-game demo:")]
	public string demoScene;
	[Header("The scene for the framework editor:")]
	public string workshopScene;

	// ------------ Called by UI buttons------------
	public void LoadDemo(){
		LoadScene (demoScene);
	}
	public void LoadWorkshop(){
		LoadScene (workshopScene);
	}
	// ---------------------------------------------

	// Loads the stated scene:
	void LoadScene(string sceneName){
		SceneManager.LoadScene (sceneName);
	}
}
