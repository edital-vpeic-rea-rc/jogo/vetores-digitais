﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// "Item Script for CustomBuilderToolkit"
/// 
/// This is for the attachment's buttons.
/// This allows the spawning of attachments that
/// are to be attached to a framework/chassis 
/// in the workshop
/// 
/// 
///       Visyde Interactives | 2017
/// </summary>

public class V_Item : MonoBehaviour {

	public V_Attachment thisSpawns;  // What attachment does this spawn?
	public Vector3 spawnPos = new Vector3(0,10,0);   // This is where the attachment will be spawned.
    private Button spawnBtn;

	// Use this for initialization
	void Start () {
        spawnBtn = GetComponent<Button>();
		//mngr = FindObjectOfType (typeof(V_WorkshopManager)) as V_WorkshopManager;
	}

	public void Select(){
		if (!V_WorkshopManager.curHolding) {
			GameObject attGobj = Instantiate (thisSpawns.gameObject, spawnPos, Quaternion.identity) as GameObject;
            V_Attachment att = attGobj.GetComponent<V_Attachment>();
            att.SpawnedBtn = spawnBtn;
            V_WorkshopManager.curHolding = att;
		}
	}
}
