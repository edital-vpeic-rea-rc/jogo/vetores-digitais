﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class V_FrameData {
	
	[Header("Framework:")]
	public int framework;

	[Header("Slots:")]
	public int[] content;
}