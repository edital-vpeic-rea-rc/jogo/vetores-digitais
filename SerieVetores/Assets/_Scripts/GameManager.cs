﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
public class GameManager : MonoBehaviour {

    public string levelToLoad;
    public Image fadePanel;
    public static GameManager instance;
    private void Awake() {
        if (instance != null) {
            Destroy(gameObject);
        } else {
            instance = this;
        }
    }
    private void Start() {
        if (fadePanel) {
            fadePanel.gameObject.SetActive(true);
            fadePanel.DOFade(0, 2);
        }
    }
    public void SetLevelToLoad(string level) {
        levelToLoad = level;
    }

    public void LoadLevel() {
        Time.timeScale = 1;
        //SceneManager.LoadScene(levelToLoad);
        LoadingScreenPro.LoadScene(levelToLoad);
    }

    public void UnpauseGame() {
        Time.timeScale = 1;
    }

    public void PauseGame() {
        Time.timeScale = 0;
    }

    void OnApplicationQuit() {
        GameGrind.Journal.Save();
    }
    public void OpenLink(string link) {
        Application.OpenURL(link);
    }
}
