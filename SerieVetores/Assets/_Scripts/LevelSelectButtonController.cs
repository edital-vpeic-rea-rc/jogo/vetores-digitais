﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectButtonController : MonoBehaviour {

    public string unlockedVetor = "MosquitoPalha";
    public string vetorfinalScoreKey = "CarrapatoScore";

    public Button btn;

    public Image background;

    public Sprite unlockeSprite;

    public GameObject star1, star2, star3;

    public bool isfirstLevel = false;

	void Start () {
       
        if (ES3.KeyExists(unlockedVetor)) {
            btn.interactable = true;
            background.sprite = unlockeSprite;
            background.color = Color.white;
            if (ES3.KeyExists(vetorfinalScoreKey))
                CheckStars(ES3.Load<int>(vetorfinalScoreKey));
            else
                CheckStars(0);
        } else {
            btn.interactable = false;
            CheckStars(0);
        }

        if (isfirstLevel) {
            btn.interactable = true;
            if (ES3.KeyExists(vetorfinalScoreKey)) {
                CheckStars(ES3.Load<int>(vetorfinalScoreKey));
                Debug.Log("Hey!");
            }
            else {
                CheckStars(0);
                Debug.Log("Hey! man");
            }
                
        }
    }

    public void CheckStars(int numStars) {
        if(numStars == 0) {
            star1.SetActive(false);
            star2.SetActive(false);
            star3.SetActive(false);
        }else if (numStars == 1) {
            star1.SetActive(true);
            star2.SetActive(false);
            star3.SetActive(false);
        }else if (numStars == 2) {
            star1.SetActive(true);
            star2.SetActive(true);
            star3.SetActive(false);
        } else if (numStars == 3) {
            star1.SetActive(true);
            star2.SetActive(true);
            star3.SetActive(true);
        }
    }
}
