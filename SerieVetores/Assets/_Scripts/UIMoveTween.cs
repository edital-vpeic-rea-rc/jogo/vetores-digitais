﻿using UnityEngine;
using DG.Tweening;

public class UIMoveTween : MonoBehaviour {

    RectTransform recT;

    [Header("In values")]
    public float durationIn = 1;

    public float delayIn;

    public Vector3 inValue;
    

    public Ease easeIn = Ease.Linear;

    [Header("Out values")]

    public float durationOut = 1;

    public float delayOut;

    public Vector3 outValue;

    public Ease easeOut = Ease.Linear;

    private void Start() {
        recT = GetComponent<RectTransform>();
    }

    public void MoveIn() {
        recT.DOLocalMove(inValue, durationIn).SetEase(easeIn).SetDelay(delayIn); 
    }
	
	public void MoveOut() {
        recT.DOLocalMove(outValue, durationOut).SetEase(easeOut).SetDelay(delayOut);
    }
}
