﻿using DoozyUI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour {

    public Image musicIcon;
    public Image sfxIcon;
    public Sprite onSprite;
    public Sprite offSprite;
    public bool isMusicON = true;
    public bool isSFXON = true;

    //public Slider SFXSlider;
    //public Slider MusicSlider;
    public Soundy soundy;
    public AudioSource aud;

    void Start () {
        //PlayerPrefs.DeleteAll ();
        soundy = FindObjectOfType<Soundy>();

  //      if (PlayerPrefs.HasKey ("sfx")) {
		//	SFXSlider.value = PlayerPrefs.GetFloat ("sfx");
  //          soundy.masterVolume = SFXSlider.value;
  //          if(aud)
  //              aud.volume = SFXSlider.value;
  //      } else {
		//	SFXSlider.value = SFXSlider.maxValue;
		//}
		//if (PlayerPrefs.HasKey ("music")) {
		//	MusicSlider.value = PlayerPrefs.GetFloat ("music");
		//} else {
		//	MusicSlider.value = MusicSlider.maxValue;
		//}
        //SoundManager.SetVolumeSFX(SFXSlider.value);
        //SoundManager.SetVolumeMusic(MusicSlider.value);

        //SFXSlider.onValueChanged.AddListener(delegate { SFXVolumeChange(); });
        //MusicSlider.onValueChanged.AddListener(delegate { MusicVolumeChange(); });

        //Mute Audio
        if (ES3.KeyExists("musicState")) {
            isMusicON = ES3.Load<bool>("musicState");
        }
            isMusicON = !isMusicON;
            
        if (ES3.KeyExists("sfxState")) {
            isSFXON = ES3.Load<bool>("sfxState");
        }
            isSFXON = !isSFXON;
            

        ToggleMusic();
        ToggleSFX();
    }

  //  public void SFXVolumeChange() {
		//PlayerPrefs.SetFloat ("sfx", SFXSlider.value);
  //      SoundManager.SetVolumeSFX(SFXSlider.value);
        
  //      soundy.masterVolume = SFXSlider.value;
  //      if (aud)
  //          aud.volume = SFXSlider.value;
  //  }

  //  public void MusicVolumeChange() {
		//PlayerPrefs.SetFloat ("music", MusicSlider.value);
  //      SoundManager.SetVolumeMusic(MusicSlider.value);
  //  }

    public void ToggleMusic() {
        isMusicON = !isMusicON;
        if (isMusicON) {
            musicIcon.sprite = onSprite;
        }
        else {
            musicIcon.sprite = offSprite;
        }
        SoundManager.MuteMusic(!isMusicON);
        ES3.Save<bool>("musicState", isMusicON);
    }
    public void ToggleSFX() {
        isSFXON = !isSFXON;
        if (isSFXON) {
            sfxIcon.sprite = onSprite;
            soundy.masterVolume = 1;
            if (aud)
                aud.volume = 1;
        } 
        else {
            sfxIcon.sprite = offSprite;
            soundy.masterVolume = 0;
            if (aud)
                aud.volume = 0;
        }
        SoundManager.MuteSFX(!isSFXON);
        ES3.Save<bool>("sfxState", isSFXON);
    }

    public void PlayMusic(string connection) {
        SoundManager.PlayConnection(connection);
    }
}
