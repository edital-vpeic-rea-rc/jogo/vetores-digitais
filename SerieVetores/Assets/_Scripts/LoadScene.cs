﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class LoadScene : MonoBehaviour {
    public GameObject loadingPanel;

	public void LoadLevel(string level) {
        Time.timeScale = 1;
        loadingPanel.SetActive(true);
        SceneManager.LoadScene(level);
    }
}
