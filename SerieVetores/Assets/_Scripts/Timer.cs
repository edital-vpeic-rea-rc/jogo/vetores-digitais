﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Timer : MonoBehaviour {
    public TextMeshProUGUI timerText;

    public float timer = 0;

    public bool timerStop;

    public static Timer instance;
    private void Awake() {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    void Update () {
        if (!timerStop) {
            timer += Time.deltaTime;

            int seconds = (int)(timer % 60);
            int minutes = (int)(timer / 60) % 60;
            //int hours = (int)(timer / 3600) % 24;

            string timerString = string.Format("{0:00}:{1:00}", minutes, seconds);
            //string timerString = string.Format("{0:0}:{1:00}:{2:00}",hours,minutes,seconds);

            timerText.text = timerString;
        }
	}

    public void ToggleTimerPause() {
        timerStop = !timerStop;
        if (timerStop) {
            timerText.color = Color.red;
        } else {
            timerText.color = Color.white;
        }
    }
}
