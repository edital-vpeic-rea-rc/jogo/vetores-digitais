﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : MonoBehaviour {

    public Texture2D normalCurso, holdingCurso, hoverCurso;

    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;

    public static CursorManager cursorManager;
    void Awake() {
        cursorManager = this;
        SetNormalCurso();
    }

    public void SetNormalCurso() {
        Cursor.SetCursor(normalCurso, hotSpot, cursorMode);
    }
    public void SetHoldingCurso() {
        Cursor.SetCursor(holdingCurso, hotSpot, cursorMode);
    }
    public void SetHoverCursor() {
        Cursor.SetCursor(hoverCurso, hotSpot, cursorMode);
    }
}
