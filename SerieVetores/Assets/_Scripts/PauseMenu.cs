﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class PauseMenu : MonoBehaviour {

    public GameObject pauseMenu;

    //public Button pauseButton,closeButton;

    public Ease ease = Ease.Linear;

    bool ispaused;
    bool isTweening;

    public void PauseGame() {
        if (!ispaused) {
            if (!isTweening)
                StartCoroutine(Pause());
        }
    }
    IEnumerator Pause() {
        isTweening = true;
        Tween myTween = pauseMenu.transform.DOScale(1, 0.5f).SetEase(ease);
        yield return myTween.WaitForCompletion();
        ispaused = true;
        isTweening = false;
        Time.timeScale = 0;
    }

    public void UnpauseGame() {
        if (ispaused) {
            if (!isTweening)
                StartCoroutine(Unpause());
        }
    }
    IEnumerator Unpause() {
        Time.timeScale = 1;
        isTweening = true;
        Tween myTween = pauseMenu.transform.DOScale(0, 0.5f).SetEase(ease);
        yield return myTween.WaitForCompletion();
        ispaused = false;
        isTweening = false;
    }


}
