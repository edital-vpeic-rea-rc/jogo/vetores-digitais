﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementDescriptionPopup : MonoBehaviour {

    public Text titleText, descriptionText;

    public Image icon;

    public static AchievementDescriptionPopup descriptionPopup;
    // Use this for initialization
    void Awake () {
        descriptionPopup = this;
    }
    public void SetDescriptionPopup(string title, string description, Sprite sprite) {
        titleText.text = title;
        descriptionText.text = description;
        icon.sprite = sprite;
    }

}
